<?php

    if (isset($_POST["ContactFormSend"])) {

        require("libs/sendgrid-php/sendgrid-php.php");

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("test@example.com", "Example User");
        $email->setSubject("PizzaContact");
        $email->addTo("dmilev22@gmail.com", "Example User");

        $email_content = "";

        if (isset($_POST['Name']) && strlen(trim($_POST['Name']))){
            $email_content .= "Name: " . $_POST['Name'] . "<br>";
        }

        if (isset($_POST['People']) && strlen(trim($_POST['People']))){
            $email_content .= "People: " . $_POST['People'] . "<br>";
        }

        if (isset($_POST['date']) && strlen(trim($_POST['date']))){
            $email_content .= "Date: " . $_POST['date'] . "<br>";
        }

        if (isset($_POST['Message']) && strlen(trim($_POST['Message']))){
            $email_content .= "Message: " . $_POST['Message'] . "<br>";
        }


        $email->addContent("text/html", $email_content);

        $sendgrid = new \SendGrid($apiKey);
        try {
            $response = $sendgrid->send($email);
            $_msg_contactFormSent = "Contact from sent, thank you";
            /*print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";*/
        } catch (Exception $e) {
            /*echo 'Caught exception: ' . $e->getMessage() . "\n";*/
            $_msg_contactFormSent = "Error";
        }
    }